const getGets = (arr) => {
  let index = 0;

  return () => {
      const toReturn = arr[index];
      index += 1;
      return toReturn;
  };
};
// this is the test
const test = [
  '6',
  '9',
  '11',
  '3',
  '2',
  '1',
  '8'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

//======================================




let numbers = gets().split(",").map(Number);

let belowAverageNumbers = [];
let aboveAverageNumbers = [];

let findAverage = numbers => numbers.reduce((a, b) => a + b, 0) / numbers.length;

let average = findAverage(numbers);

for (let i = 0; i < numbers.length; i++) {
    let currentNumber = numbers[i];

    if (currentNumber > average) {
        aboveAverageNumbers.push(currentNumber);
    } else if (currentNumber === 0 && average === 0) {
        continue;
    } else {
        belowAverageNumbers.push(currentNumber);
    }
}

print(`avg: ${average.toFixed(2)}`);
print(`below: ${belowAverageNumbers}`);
print(`above: ${aboveAverageNumbers}`);
