const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  // this is the test
  const test = [
    '5,3,2,1',
    '2'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

 // ======================================


  let nums = gets().split(",").map(Number);
  let index = +gets();

  while (index--) {
      nums.push(nums.shift())
  }

  print(nums.join(","));