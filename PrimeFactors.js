const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  // this is the test
  const test = [
    '12'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

 // ======================================

 let n = +gets();

function primeFactors(n){
    let factors = [] 
    let divisor = 2;
  
    while(n>2){
      if(n % divisor == 0){
         factors.push(divisor); 
         n = n / divisor;
      }
      else{
        divisor++;
      }     
    }
    return factors;
  }

  for (i = 0; i < primeFactors(n).length; i++) {
    print(primeFactors(n)[i])
  }
  
  



