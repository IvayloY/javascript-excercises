const getGets = (arr) => {
    let index = 0;
  
    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
  };
  // this is the test
  const test = [
    '0,0,0,5,0,3,2,3'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

 // ======================================

let n = gets();
let arr = n.split(",").map(Number);
let zeroCounter = 0;


for(i = 0; i < arr.length; i++) {
    if (arr[i] ===  0)  {
        zeroCounter++
      
         
    }
    
}

let filtered = arr.filter(function(value){

    return value != 0;

});


for (j = 0; j < zeroCounter; j++) {
    filtered.push(0);
}

print(filtered.join(","));